package main

import (
	"testing"

	_ "github.com/apatters/go-trace"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	testSchemaVersionStr      = "1.2"
	testBadSchemaVersionStr   = "1.s"
	testSemanticVersionStr    = "1.2.3"
	testBadSemanticVersionStr = "1.1"
)

var (
	testSchemaVersionObj = SchemaVersion{
		MajorNumber: 1,
		MinorNumber: 2,
	}
	testSemanticVersionObj = SemanticVersion{
		MajorNumber: 1,
		MinorNumber: 2,
		PatchLevel:  3,
	}
)

func TestSchemaVersionMarshalJSON(t *testing.T) {
	data, err := testSchemaVersionObj.MarshalJSON()
	require.NoError(t, err)
	assert.Equal(t, string(data), "\""+testSchemaVersionStr+"\"")
}

func TestSchemaVersionUnmarshalJSON(t *testing.T) {

	// Test a good one.
	var sv SchemaVersion
	err := sv.UnmarshalJSON([]byte(testSchemaVersionStr))
	require.NoError(t, err)
	assert.Equal(t, sv, testSchemaVersionObj)

	// Test a bad one.
	err = sv.UnmarshalJSON([]byte(testBadSchemaVersionStr))
	require.Error(t, err)
}

func TestSemanticVersionMarshalJSON(t *testing.T) {
	data, err := testSemanticVersionObj.MarshalJSON()
	require.NoError(t, err)
	assert.Equal(t, string(data), "\""+testSemanticVersionStr+"\"")
}

func TestSemanticVersionUnmarshalJSON(t *testing.T) {

	// Test a good one.
	var sv SemanticVersion
	err := sv.UnmarshalJSON([]byte(testSemanticVersionStr))
	require.NoError(t, err)
	assert.Equal(t, sv, testSemanticVersionObj)

	// Test a bad one.
	err = sv.UnmarshalJSON([]byte(testBadSemanticVersionStr))
	require.Error(t, err)
}
