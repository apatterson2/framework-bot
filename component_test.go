package main

import (
	"testing"

	_ "github.com/apatters/go-trace"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	testComponentObj = Component{
		ID:      ComponentGolang,
		Version: "1.21.7",
		EpicURL: nil,
	}
)

func TestComponentString(t *testing.T) {
	assert.Equal(t, testComponentObj.String(), "golang 1.21.7")
}

func TestComponentEnumMarshalJSON(t *testing.T) {
	data, err := ComponentGolang.MarshalJSON()
	require.NoError(t, err)
	assert.Equal(t, string(data), `"golang"`)
}

func TestComponentEnumUnmarshalJSON(t *testing.T) {
	var c ComponentEnum
	err := c.UnmarshalJSON([]byte("golang"))
	require.NoError(t, err)
	assert.Equal(t, c, ComponentGolang)
}

//func TestCreateOrReopenEpic(t *testing.T) {
//	gc, err := gitlabclient.NewGitlabClient()
//	require.NoError(t, err)
//	assert.NotNil(t, gc)

//	testComponentObj.EpicURL = utils.ParseURLNoError("https://gitlab.com/gitlab-org/distribution")
//	err = testComponentObj.CreateOrReopenEpic(gc)
//	assert.NoError(t, err)
//}
