package main

import (
	"fmt"
	"net/url"
	"os"
	"strconv"
)

func GetEnvStr(key string) (string, error) {
	value, exists := os.LookupEnv(key)
	if !exists {
		return "", fmt.Errorf("unable to find environment variable '%s'", key)
	}
	return value, nil
}

func GetEnvInt(key string) (int, error) {
	valueStr, exists := os.LookupEnv(key)
	if !exists {
		return 0, fmt.Errorf("unable to find environment variable `%s`", key)
	}
	valueNum, err := strconv.Atoi(valueStr)
	if err != nil {
		return 0, fmt.Errorf(
			"cannot convert environment variable `%s` with contents '%s' to integer: %w",
			key,
			valueStr,
			err)
	}
	return valueNum, nil
}

func GetEnvStrWithDefault(key string, defaultValue string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		return defaultValue
	}
	return value
}

func ParseURLNoError(s string) *url.URL {
	url, err := url.Parse(s)
	if err != nil {
		return nil
	}

	return url
}
