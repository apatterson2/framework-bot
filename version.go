package main

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
)

var (
	schemaVersionRegexp   = regexp.MustCompile(`(\d+)\.(\d+)`)
	semanticVersionRegexp = regexp.MustCompile(`(\d+)\.(\d+)\.(\d)+`)
)

type SchemaVersion struct {
	MajorNumber uint
	MinorNumber uint
}

func (v SchemaVersion) String() string {
	return fmt.Sprintf("%d.%d", v.MajorNumber, v.MinorNumber)
}

func (v SchemaVersion) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.String())
}

func (v *SchemaVersion) UnmarshalJSON(data []byte) error {
	match := schemaVersionRegexp.FindStringSubmatch(string(data))
	if match == nil || len(match) != 3 {
		return fmt.Errorf("cannot parse '%s' as 'schema_version'", string(data))
	}

	majorNumber, _ := strconv.ParseUint(match[1], 10, 0)
	minorNumber, _ := strconv.ParseUint(match[2], 10, 0)
	v.MajorNumber = uint(majorNumber)
	v.MinorNumber = uint(minorNumber)

	return nil
}

type SemanticVersion struct {
	MajorNumber uint
	MinorNumber uint
	PatchLevel  uint
}

func (v SemanticVersion) String() string {
	return fmt.Sprintf("%d.%d.%d", v.MajorNumber, v.MinorNumber, v.PatchLevel)
}

func (v SemanticVersion) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.String())
}

func (v *SemanticVersion) UnmarshalJSON(data []byte) error {
	match := semanticVersionRegexp.FindStringSubmatch(string(data))
	if match == nil || len(match) != 4 {
		return fmt.Errorf("cannot parse '%s' as 'semantic_version'", string(data))
	}

	majorNumber, _ := strconv.ParseUint(match[1], 10, 0)
	minorNumber, _ := strconv.ParseUint(match[2], 10, 0)
	patchLevel, _ := strconv.ParseUint(match[3], 10, 0)
	v.MajorNumber = uint(majorNumber)
	v.MinorNumber = uint(minorNumber)
	v.PatchLevel = uint(patchLevel)

	return nil
}
