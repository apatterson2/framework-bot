package main

import (
	"encoding/json"
	"testing"

	_ "github.com/apatters/go-trace"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	testStreamConfigJSONStr = `{
  "schema_version": "1.2",
  "build_stream_version": "5.1.0",
  "components": {
    "golang": {
      "version": "1.21.7",
      "epic_url": "https://gitlab.com/gitlab-org/distribution/build-compliance"
    },
    "ruby": {
      "version": "3.1.4",
      "epic_url": "https://gitlab.com/gitlab-org/distribution/build-compliance"
    }
  }
}`
)

var (
	testStreamConfigJSONObj = streamConfigJSON{
		SchemaVersion: SchemaVersion{
			MajorNumber: 1,
			MinorNumber: 2,
		},
		BuildStreamVersion: SemanticVersion{
			MajorNumber: 5,
			MinorNumber: 1,
			PatchLevel:  0,
		},
		Components: map[string]ComponentJSON{
			"golang": {
				Version: "1.21.7",
				EpicURL: "https://gitlab.com/gitlab-org/distribution/build-compliance",
			},
			"ruby": {
				Version: "3.1.4",
				EpicURL: "https://gitlab.com/gitlab-org/distribution/build-compliance",
			},
		},
	}

	testStreamConfigObj = StreamConfig{
		SchemaVersion: SchemaVersion{
			MajorNumber: 1,
			MinorNumber: 2,
		},
		BuildStreamVersion: SemanticVersion{
			MajorNumber: 5,
			MinorNumber: 1,
			PatchLevel:  0},
		Components: map[ComponentEnum]Component{
			ComponentGolang: {
				ID:      ComponentGolang,
				Version: "1.21.7",
				EpicURL: ParseURLNoError("https://gitlab.com/gitlab-org/distribution/build-compliance"),
			},
			ComponentRuby: {
				ID:      ComponentRuby,
				Version: "3.1.4",
				EpicURL: ParseURLNoError("https://gitlab.com/gitlab-org/distribution/build-compliance"),
			},
		},
	}
)

func TestStreamConfigJSONUnmarshal(t *testing.T) {
	obj := streamConfigJSON{}
	err := json.Unmarshal([]byte(testStreamConfigJSONStr), &obj)
	require.NoError(t, err)
	assert.Equal(t, obj, testStreamConfigJSONObj)
}

func TestStreamConfigJSONMarshal(t *testing.T) {
	data, err := json.MarshalIndent(testStreamConfigJSONObj, "", "  ")
	require.NoError(t, err)
	assert.Equal(t, testStreamConfigJSONStr, string(data))
}

func TestStreamConfigUnmarshal(t *testing.T) {
	var sc StreamConfig
	err := sc.UnmarshalJSON([]byte(testStreamConfigJSONStr))
	require.NoError(t, err)
	assert.Equal(t, sc, testStreamConfigObj)
}

func TestStreamConfigUnmarshalJSON(t *testing.T) {
	data, err := testStreamConfigObj.MarshalJSON()
	require.NoError(t, err)
	assert.JSONEq(t, string(data), testStreamConfigJSONStr)
}

func TestStreamConfigUnmarshalJSONIndent(t *testing.T) {
	data, err := testStreamConfigObj.MarshalJSONIndent("", "  ")
	require.NoError(t, err)
	assert.Equal(t, string(data), testStreamConfigJSONStr)
}
