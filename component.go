package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"

	_ "github.com/apatters/go-trace"
)

type ComponentEnum uint

const (
	ComponentUnknown ComponentEnum = iota
	ComponentGolang
	ComponentRuby
)

func (c ComponentEnum) String() string {
	switch c {
	case ComponentGolang:
		return "golang"
	case ComponentRuby:
		return "ruby"
	}

	return "unknown"
}

func ParseComponent(s string) ComponentEnum {
	switch s {
	case "golang":
		return ComponentGolang
	case "ruby":
		return ComponentRuby
	}

	return ComponentUnknown
}

func (c ComponentEnum) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.String())
}

func (c *ComponentEnum) UnmarshalJSON(data []byte) error {
	*c = ParseComponent(string(data))
	return nil
}

type ComponentJSON struct {
	Version string `json:"version"`
	EpicURL string `json:"epic_url"`
}

type Component struct {
	ID      ComponentEnum
	Version string
	EpicURL *url.URL
}

func (c Component) String() string {
	return fmt.Sprintf("%v %v", c.ID, c.Version)
}

func (c Component) CreateOrReopenEpic(gc *GitlabClient) error {
	if c.EpicURL == nil {
		return fmt.Errorf("no URL for component '%v'", c)
	}

	title := fmt.Sprintf(EpicTitleTemplate, c.String())
	description := fmt.Sprintf(EpicDescTemplate, c.ID, c.Version)

	epic, err := gc.FindEpic(c.EpicURL, title)
	if err != nil {
		return err
	}
	if epic == nil {
		log.Printf("no existing epic in group '%v'\n", c.EpicURL)
		epic, err = gc.CreateEpic(c.EpicURL, title, description)
		if err != nil {
			return fmt.Errorf("cannot create epic: %w", err)
		}
		log.Println("epic created:", epic.WebURL)
		return nil
	} else {
		log.Printf("found existing epic '%v'\n", epic.WebURL)
		switch epic.State {
		case "closed":
			err = gc.ReopenEpic(c.EpicURL, epic)
			log.Printf("epic '%s' is closed -- reopened\n", epic.WebURL)
			return err
		case "opened":
			log.Printf("epic '%s' is already open\n", epic.WebURL)
			return nil
		default:
			return fmt.Errorf("epic in invalid error state '%v', must be 'opened' or 'closed", epic.State)
		}
	}
}
