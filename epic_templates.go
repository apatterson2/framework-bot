package main

const (
	EpicTitleTemplate = "Update to build framework component %s"
	EpicDescTemplate  = `
The build framework component %s has been updated to %s. This epic
tracks progress for qualifying projects that use this component.
`
)
