package main

import (
	"fmt"
	"net/url"

	_ "github.com/apatters/go-trace"
	"github.com/xanzy/go-gitlab"
)

const (
	defaultBaseURL  = "https://gitlab.com"
	BaseURLEnvName  = "ISSUE_TRACKER_BASE_URL"
	APITokenEnvName = "ISSUE_TRACKER_API_TOKEN"
)

type GitlabClient struct {
	gitlabClient *gitlab.Client
}

func NewGitlabClient() (*GitlabClient, error) {
	url := GetEnvStrWithDefault(BaseURLEnvName, defaultBaseURL)
	token, err := GetEnvStr(APITokenEnvName)
	if err != nil {
		return nil, fmt.Errorf("environment variable `%s` not set", APITokenEnvName)
	}

	gc, err := gitlab.NewClient(token, gitlab.WithBaseURL(url))
	if err != nil {
		return nil, fmt.Errorf("unable to create client: %w", err)
	}

	client := &GitlabClient{
		gitlabClient: gc,
	}

	return client, nil
}

func (gc GitlabClient) FindEpic(url *url.URL, title string) (*gitlab.Epic, error) {
	opts := &gitlab.ListGroupEpicsOptions{
		Search: &title,
	}
	path := urlToGroupPath(url)
	epics, _, err := gc.gitlabClient.Epics.ListGroupEpics(path, opts)
	if err != nil {
		return nil, fmt.Errorf("unable to find epic in group '%s': %w", path, err)
	}
	if len(epics) == 0 {
		return nil, nil
	}
	return epics[0], nil
}

func (gc GitlabClient) CreateEpic(url *url.URL, title string, description string) (*gitlab.Epic, error) {
	opts := &gitlab.CreateEpicOptions{
		Title:       &title,
		Description: &description,
	}
	path := urlToGroupPath(url)
	epic, _, err := gc.gitlabClient.Epics.CreateEpic(path, opts)
	if err != nil {
		return nil, fmt.Errorf("unable to create epic in group '%s': %w", path, err)
	}

	return epic, nil
}

func (gc GitlabClient) ReopenEpic(url *url.URL, epic *gitlab.Epic) error {
	reopenEvent := "reopen"
	opts := &gitlab.UpdateEpicOptions{
		StateEvent: &reopenEvent,
	}
	path := urlToGroupPath(url)
	_, _, err := gc.gitlabClient.Epics.UpdateEpic(path, epic.ID, opts)
	if err != nil {
		return fmt.Errorf("unable to reopen epic in group '%s': %w", path, err)
	}

	return nil

}

func urlToGroupPath(url *url.URL) string {
	// Remove leading "/" from group path if it exists.
	path := url.Path
	if path[0] == '/' {
		path = path[1:]
	}

	return path
}
