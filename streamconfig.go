package main

import (
	"encoding/json"
	"fmt"
	"net/url"
)

const (
	DefaultPath = "/etc/framework/stream_config.json"
)

type streamConfigJSON struct {
	SchemaVersion      SchemaVersion            `json:"schema_version"`
	BuildStreamVersion SemanticVersion          `json:"build_stream_version"`
	Components         map[string]ComponentJSON `json:"components"`
}

type StreamConfig struct {
	SchemaVersion      SchemaVersion
	BuildStreamVersion SemanticVersion
	Components         map[ComponentEnum]Component
}

func (sc StreamConfig) MarshalJSON() ([]byte, error) {
	return sc.MarshalJSONIndent("", "")
}

func (sc StreamConfig) MarshalJSONIndent(prefix string, indent string) ([]byte, error) {
	var scJSONObj streamConfigJSON

	scJSONObj.SchemaVersion = sc.SchemaVersion
	scJSONObj.BuildStreamVersion = sc.BuildStreamVersion
	scJSONObj.Components = make(map[string]ComponentJSON)
	for key, val := range sc.Components {
		id := key.String()
		comp := ComponentJSON{
			Version: val.Version,
			EpicURL: val.EpicURL.String(),
		}
		scJSONObj.Components[id] = comp
	}

	type streamConfigJSONAlias streamConfigJSON // Prevent recursion.
	return json.MarshalIndent(streamConfigJSONAlias(scJSONObj), prefix, indent)
}

func (sc *StreamConfig) UnmarshalJSON(data []byte) error {
	var scJSONObj streamConfigJSON
	err := json.Unmarshal(data, &scJSONObj)
	if err != nil {
		return fmt.Errorf("cannot unmarshal steam config: %w", err)
	}
	sc.SchemaVersion = scJSONObj.SchemaVersion
	sc.BuildStreamVersion = scJSONObj.BuildStreamVersion
	clear(sc.Components)
	sc.Components = make(map[ComponentEnum]Component)
	for key, val := range scJSONObj.Components {
		url, _ := url.Parse(val.EpicURL)
		if err != nil {
			return fmt.Errorf("cannot parse epic_url '%s': %w", val.EpicURL, err)
		}
		id := ParseComponent(key)
		comp := Component{
			ID:      id,
			Version: val.Version,
			EpicURL: url,
		}
		sc.Components[id] = comp
	}

	return nil
}
